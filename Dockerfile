FROM ubuntu

RUN sed -i 's/# deb/deb/g' /etc/apt/sources.list

RUN apt-get update \
    && apt-get install -y apt-utils sudo systemd systemd-sysv \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN cd /lib/systemd/system/sysinit.target.wants/ \
    && ls | grep -v systemd-tmpfiles-setup | xargs rm -f $1

RUN rm -f /lib/systemd/system/multi-user.target.wants/* \
    /etc/systemd/system/*.wants/* \
    /lib/systemd/system/local-fs.target.wants/* \
    /lib/systemd/system/sockets.target.wants/*udev* \
    /lib/systemd/system/sockets.target.wants/*initctl* \
    /lib/systemd/system/basic.target.wants/* \
    /lib/systemd/system/anaconda.target.wants/* \
    /lib/systemd/system/plymouth* \
    /lib/systemd/system/systemd-update-utmp*

RUN apt-get -y update && apt-get -y upgrade

RUN apt-get -y install busybox dnsutils iproute2 net-tools iputils-ping iptables network-manager cron rsyslog isc-dhcp-client isc-dhcp-server isc-dhcp-common procps

RUN apt-get -y install nano curl perl wget build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev openssl openssh-server openssh-client 

RUN apt-get -y install python python3 python3-pip python3-dev
RUN mkdir /var/run/sshd

RUN sed -ri 's/^#?PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config

RUN export PASSWORD=$PASSWORD DYNV6_HOSTNAME=$DYNV6_HOSTNAME DYNV6_TOKEN=$DYNV6_TOKEN

RUN mkdir /root/.ssh

RUN mkdir /root/.app

COPY ./dynv6.sh /root/.app/

RUN chmod +x /root/.app/dynv6.sh

COPY ./app.py /root/.app/

COPY ./script.sh /root/.app/

RUN chmod +x /root/.app/script.sh

COPY requirements.txt /root/.app/

RUN pip3 install -r /root/.app/requirements.txt

RUN cd /root

WORKDIR /root

EXPOSE 8080

EXPOSE 22

CMD ["python3", "/root/.app/app.py"]

VOLUME [ "/sys/fs/cgroup" ]

CMD ["/lib/systemd/systemd"]

CMD ["/usr/sbin/sshd", "-D"]