#! /bin/bash

echo './script.sh execution is started!'

sudo echo root:$PASSWORD | chpasswd

#sysctl net.ipv4.conf.all.forwarding=1

#sudo iptables -P FORWARD ACCEPT

echo 'Step 1/2'

#some configurations

ip addr show scope global | grep 'inet ' | awk '{print $2}' | cut -f1 -d'/'

ip addr show scope global | grep 'inet6 ' | awk '{print $2}' | cut -f1 -d'/'

#duckdns usage
#bash /root/.duckdns/duck.sh

#nsupdate usage
#bash /root/.app/update.sh

#dynv6 usage
token=$DYNV6_TOKEN /root/.app/dynv6.sh $DYNV6_HOSTNAME

(crontab -l; echo "*/1 * * * * /srv/dynv6-cron-v6only.sh $DYNV6_HOSTNAME $DYNV6_TOKEN > /dev/null 2>&1") | sort -u | crontab - && (crontab -l; echo "*/1 * * * * /srv/dynv6-cron-v4only.sh $DYNV6_HOSTNAME $DYNV6_TOKEN > /dev/null 2>&1") | sort -u | crontab - && crontab -l

sudo echo ALL >>/etc/cron.deny && sudo echo ishmael >>/etc/cron.allow

service cron start

netstat -tulpn | grep LISTEN

ps ax | grep app.py

#ifconfig -a

echo 'Step 2/2'

appname='Bot3'
echo $appname 'is ready to serve!'

echo './script.sh execution is complete!'